﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Innovix_Task.Models
{
    public class Reservation
    {
        public int Id { get; set; }
        public byte NumberOfGuests { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public int Cost { get; set; }
        public int MealPlanId { get; set; }
        public MealPlan MealPlan { get; set; }
        public ICollection<Room> Rooms { get; set; }

    }
}
