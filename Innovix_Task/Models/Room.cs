﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Innovix_Task.Models
{
    public class Room
    {
        public int Id { get; set; }
        public RoomType Type { get; set; }
        public bool IsHighSeason { get; set; }
        public int Cost { get; set; }
        public int Capacity { get; set; }
        public int MealPlanId { get; set; }
        public MealPlan MealPlan { get; set; }
        public int ReservationId { get; set; }
        public Reservation Reservation { get; set; }
    }
}
