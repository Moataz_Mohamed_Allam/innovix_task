﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Innovix_Task.Models
{
    public class MealPlan
    {
        public int Id { get; set; }
        public string MealType { get; set; }
        public int Cost { get; set; }
        public bool IsHighSeason { get; set; }
    }
}
