﻿using Innovix_Task.Models;
using System;
using System.Linq;


namespace Innovix_Task.BusinessLogic
{
    public class TotalCalculator
    {
        private readonly ApplicationContext _context;

        public TotalCalculator(ApplicationContext context)
        {
            _context = context;
        }
        
        public double GetReservationTotal(DateTime checkInDate, DateTime checkOutDate,
                                              int noOfGuests, RoomType roomType, MealPlan mealPlan)
        {
            double total = 0;
            double reservedDays = (checkOutDate - checkInDate).TotalDays;
         
            if (!mealPlan.IsHighSeason)
            {
                var roomTypeCostPerNight = _context.RoomTypes
                    .Where(r => r.Type == roomType.Type
                            && r.From.Date == roomType.From.Date
                            && r.To.Date == roomType.To.Date)
                    .Select(r => r.RatePerPerson)
                    .FirstOrDefault();


                var mealPlanCostPerDay = _context.MealPlans
                    .Where(m => m.IsHighSeason == false && m.MealType == mealPlan.MealType)
                    .Select(m => m.Cost)
                    .FirstOrDefault();

                var MealCost = mealPlanCostPerDay * noOfGuests;

                total = (roomTypeCostPerNight + MealCost) * reservedDays;

                return total;
            }

            else if (mealPlan.IsHighSeason)
            {
                var roomTypeCostPerNight = _context.RoomTypes
                    .Where(r => r.Type == roomType.Type
                            && r.From.Date == roomType.From.Date
                            && r.To.Date == roomType.To.Date)
                    .Select(r => r.RatePerPerson)
                    .FirstOrDefault();


                var mealPlanCostPerDay = _context.MealPlans
                    .Where(m => m.IsHighSeason == true && m.MealType == mealPlan.MealType)
                    .Select(m => m.Cost)
                    .FirstOrDefault();

                var mealCostPerDay = mealPlanCostPerDay * noOfGuests; 

                total = (roomTypeCostPerNight + mealCostPerDay) * reservedDays;

                return total;
            }
            else
            {
                return 0;
            }
        }
    }
}
