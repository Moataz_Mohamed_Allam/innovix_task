﻿using Innovix_Task.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Innovix_Task.ViewModels
{
    public class ReservationViewModel
    {
        public string Name { get; set; }
        
        public string Email { get; set; }
        
        public string Country { get; set; }
        [Range(1,2)]
        public int Adults { get; set; }
        
        [Range(1,2)]
        public int Childrens { get; set; }
        
        public bool IsHighSeason { get; set; }
       
        public string RoomType { get; set; }
        
        public RType Type { get; set; }
       
        public string MealType { get; set; }
        
        public IEnumerable<MealPlan> MealPlans { get; set; }
        
        public DateTime CheckInDate { get; set; }
        
        public DateTime CheckOutDate { get; set; }
    }
}
