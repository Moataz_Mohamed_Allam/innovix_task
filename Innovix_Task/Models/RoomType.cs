﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Innovix_Task.Models
{
    public class RoomType
    {
        public int Id { get; set; }
        public RType Type { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public int RatePerPerson { get; set; }

    }

    public enum RType
    {
        SeaView,
        PoolView,
        GardenView,
        RoyalSuite
    }
}
