﻿using Innovix_Task.BusinessLogic;
using Innovix_Task.Models;
using Innovix_Task.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Linq;

namespace Innovix_Task.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationContext _context;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ApplicationContext context, ILogger<HomeController> logger)
        {
            _context = context;
            _logger = logger;
        }

        public IActionResult Total()
        {
            var viewModel = new ReservationViewModel
            {
                MealPlans = _context.MealPlans.ToList()
            };

            return View(viewModel);
        }
        
        [HttpPost]
        public IActionResult ReservationTotal(ReservationViewModel viewModel)
        {
            var calculator = new TotalCalculator(_context);

            if(ModelState.IsValid)
            {
                var roomType = _context.RoomTypes
                   .Where(rt => rt.Type == viewModel.Type
                           && rt.From.Date == viewModel.CheckInDate
                           && rt.To.Date == viewModel.CheckOutDate)
                   .FirstOrDefault();

                var mealType = _context.MealPlans
                    .Where(mp => mp.MealType == viewModel.MealType && mp.IsHighSeason == viewModel.IsHighSeason)
                    .FirstOrDefault();

                var total = calculator.GetReservationTotal(viewModel.CheckInDate,
                                                viewModel.CheckOutDate,
                                                viewModel.Adults + viewModel.Childrens,
                                                roomType,
                                                mealType);

                TempData["total"] = JsonConvert.SerializeObject(total);

                return RedirectToAction(nameof(Result));
            }

            return View(viewModel);
           
        }

        public IActionResult Result()
        {
            var total = JsonConvert.DeserializeObject(TempData["total"].ToString());
            var result = $"The Total Amount is { total }$";

            return Content(result);
        }

        public IActionResult Index()
        {
            return View();
        }
     
    }
}
